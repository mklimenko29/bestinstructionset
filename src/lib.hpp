#pragma once

#include <cstddef>
#include <cstdint>

namespace lib {
	using type = float;

	extern "C" void Add(const type * a, const type * b, std::size_t length, type * dst);
	extern "C" std::size_t CountSetBits(std::size_t a);
}