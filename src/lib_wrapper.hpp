#pragma once

#include "lib.hpp"
#include "wrapper_common.hpp"
#include "cpuinfo_x86.h"

#include <string>
#include <string_view>
#include <vector>

#define LIBWRAPPER_ASSIGN(val) Assign(#val, val);

struct LibWrapper {
	decltype(lib::Add)* Add = nullptr;
	decltype(lib::CountSetBits)* CountSetBits = nullptr;

	LibWrapper() : LibWrapper(GetSuffix()) {}

	LibWrapper(const std::string& suffix) {
		SwitchImplementation(suffix);
	}

	~LibWrapper() {
		DllWrapper::FreeInstance(instance);
	}
	
	static auto GetAllimplementations() {
		std::vector<LibWrapper> dst;
		dst.reserve(4);

		auto suffixes = GetAllSuffixes();
		for (auto& el : suffixes)
			dst.emplace_back(el);

		return dst;
	}

	void SwitchImplementation(const std::string& suffix) {
		DllWrapper::FreeInstance(instance);

		auto path = std::string("best_instruction_set") + (suffix.empty() ? "" : ("_" + suffix));

#ifdef _WIN32
		path += ".dll";
#elif __linux__
		path = "lib" + path + ".so";
#else
		throw std::runtime_error("Unexpected system");
#endif
		instance = DllWrapper::GetInstance(path.c_str());
		if (!instance)
			throw std::runtime_error("Unable to load library " + std::string(path));

		LIBWRAPPER_ASSIGN(Add);
		LIBWRAPPER_ASSIGN(CountSetBits);
	}

private:
	DllWrapper::InstanceType instance = nullptr;
	
	template <typename T>
	void Assign(const char* symbol_name, T& dst_pointer) {
		auto address = DllWrapper::GetAddress(instance, symbol_name);
		if (!address)
			throw std::runtime_error("Unable to find symbol: " + std::string(symbol_name));

		dst_pointer = reinterpret_cast<T>(address);
	}

	static auto GetAllSuffixes() -> std::vector<std::string> {
		std::vector<std::string> dst(1); // default = "";

		const auto features = cpu_features::GetX86Info().features;

		if (features.avx)
			dst.emplace_back("avx");
		if (features.avx2)
			dst.emplace_back("avx2");
		if (features.avx512f)
			dst.emplace_back("avx512");

		return dst;
	}
		
	static auto GetSuffix() -> std::string {
		return GetAllSuffixes().back();
	}
};
