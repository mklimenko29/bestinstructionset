#include "lib_wrapper.hpp"

#include <iostream>
#include <vector>

int main() {
	try {
		std::vector<lib::type> a(64, 1);
		std::vector<lib::type> b(64, 2);
		std::vector<lib::type> dst(64);

		auto wrapper = LibWrapper();
		wrapper.Add(&a[0], &b[0], a.size(), &dst[0]);
		wrapper.CountSetBits(0xF);

		auto all_wrappers = LibWrapper::GetAllimplementations();

		for(auto& cur_wrapper : all_wrappers)
			cur_wrapper.Add(&a[0], &b[0], a.size(), &dst[0]);
		
//		wrapper.SwitchImplementation("avx512");
//		wrapper.Add(&a[0], &b[0], a.size(), &dst[0]);
//		wrapper.CountSetBits(0xF);
	}
	catch (const std::exception& e) {
		std::cerr << e.what() << std::endl;
	}
	return 0;
}