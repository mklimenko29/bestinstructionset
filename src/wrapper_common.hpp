#pragma once
#include <stdexcept>
#include <string>
#ifdef _WIN32
#include <Windows.h>
#else
#include <dlfcn.h>
#endif

namespace DllWrapper {
#ifdef _WIN32
	using InstanceType = HMODULE;
#else
	using InstanceType = void*;
#endif

	inline auto GetInstance(const char* path) {
#ifdef _WIN32
		return LoadLibraryExA(path, nullptr, 0);
#else
        return dlopen(path, RTLD_LAZY);
#endif
	}

	inline void FreeInstance(InstanceType instance) {
		if (!instance)
			return;
#ifdef _WIN32
		FreeLibrary(instance);
#else
		dlclose(instance);
#endif
	}

	inline auto GetAddress(InstanceType instance, const char* symbol_name) {
#ifdef _WIN32
		return GetProcAddress(instance, symbol_name);
#else
		return dlsym(instance, symbol_name);
#endif
	}
}

