#include <benchmark/benchmark.h>
#include "lib_wrapper.hpp"

#include <tuple>

namespace {
	auto GetVectors(benchmark::State& state) {
        std::vector<lib::type> a(state.range(), 1);
        std::vector<lib::type> b(state.range(), 2);
        std::vector<lib::type> dst(state.range());

        return std::make_tuple(std::move(a), std::move(b), std::move(dst));
	}
}

static void Add(benchmark::State& state) {
    auto lib = LibWrapper("");
    auto [a, b, dst] = GetVectors(state);
    for (auto _ : state) {
        lib.Add(&a[0], &b[0], a.size(), &dst[0]);
    }
}
BENCHMARK(Add)->RangeMultiplier(2)->Range(1, 8 << 12);

static void Add_avx(benchmark::State& state) {
    auto lib = LibWrapper("avx");
    auto [a, b, dst] = GetVectors(state);
    for (auto _ : state) {
        lib.Add(&a[0], &b[0], a.size(), &dst[0]);
    }
}
BENCHMARK(Add_avx)->RangeMultiplier(2)->Range(1, 8 << 12);

static void Add_avx2(benchmark::State& state) {
    auto lib = LibWrapper("avx2");
    auto [a, b, dst] = GetVectors(state);
    for (auto _ : state) {
        lib.Add(&a[0], &b[0], a.size(), &dst[0]);
    }
}
BENCHMARK(Add_avx2)->RangeMultiplier(2)->Range(1, 8 << 12);

int main(int argc, char** argv) {
    ::benchmark::Initialize(&argc, argv);
    if (::benchmark::ReportUnrecognizedArguments(argc, argv)) 
        return 1;
    ::benchmark::RunSpecifiedBenchmarks();

	system("pause");
}