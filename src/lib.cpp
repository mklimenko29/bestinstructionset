#include <numeric>
#include "lib.hpp"

extern "C" void lib::Add(const lib::type* a, const lib::type* b, std::size_t length, lib::type* dst) {
    for (std::size_t i = 0; i < length; ++i)
		dst[i] = a[i] + b[i];
}

extern "C" std::size_t CountSetBits(std::size_t a) {
    std::size_t count = 0;
    while (a) {
        ++count;
        a &= (a - 1);
    }
    return count;
}
